#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import log_analyzer


class MyTest(unittest.TestCase):

    def test_setting(self):
        res = log_analyzer.setting("./test/def.config")
        self.assertEqual(res, {"REPORT_SIZE": 1000,
                                "REPORT_DIR": "./reports",
                                "LOG_DIR": "./log",
                                "FILES_DEFAULT_SETTING": "default.config",
                                "LOGGING_FILENAME": "LOG_ANALIZER",
                                "PERCENT_ERROR": 60})

    def test_last_files(self):
        res = log_analyzer.last_file("./test")
        self.assertEqual(res, None)

    def test_parser(self):
        content = ("""1.140.178.176 -  - [29/Jun/2017:04:42:25 +0300] "GET /api/v2/internal/html5/phantomjs/queue/?wait=1m HTTP/1.1" 200 22 "-" "Python-urllib/2.7" "-" "1498700545-1643579065-4707-9853341" "82545a302c27186d07" 60.093\n1.126.153.80 -  - [29/Jun/2017:04:42:25 +0300] "GET /api/v2/banner/25839458/statistic/outgoings/?date_from=2017-06-28&date_to=2017-06-28 HTTP/1.1" 200 40 "-" "-" "-" "1498700545-48424485-4707-9853342" "1835ae0f17f" 0.076\n1.126.153.80 -  - [29/Jun/2017:04:42:26 +0300] "GET /api/v2/banner/22199839/statistic/outgoings/?date_from=2017-06-28&date_to=2017-06-28 HTTP/1.1" 200 40 "-" "-" "-" "1498700545-48424485-4707-9853344" "1835ae0f17f" 0.071\n1.169.137.128 -  - [29/Jun/2017:04:42:26 +0300] "GET /api/v2/group/5775925/banners HTTP/1.1" 200 1136 "-" "Configovod" "-" "1498700545-2118016444-4707-9853337" "712e90144abee9" 0.404""")
        res = log_analyzer.parser(content)
        self.assertEqual(res, [{'count': 1, 'time_avg': 60.093, 'time_max': 60.093, 'time_sum': 60.093, 'url': '/api/v2/internal/html5/phantomjs/queue/?wait=1m', 'time_med': 60.093, 'time_perc': 99.09141877184882, 'count_perc': 25.0}, {'count': 1, 'time_avg': 0.404, 'time_max': 0.404, 'time_sum': 0.404, 'url': '/api/v2/group/5775925/banners', 'time_med': 0.404, 'time_perc': 0.6661829694611173, 'count_perc': 25.0}, {'count': 1, 'time_avg': 0.076, 'time_max': 0.076, 'time_sum': 0.076, 'url': '/api/v2/banner/25839458/statistic/outgoings/?date_from=2017-06-28&date_to=2017-06-28', 'time_med': 0.076, 'time_perc': 0.1253215487105072, 'count_perc': 25.0}, {'count': 1, 'time_avg': 0.071, 'time_max': 0.071, 'time_sum': 0.071, 'url': '/api/v2/banner/22199839/statistic/outgoings/?date_from=2017-06-28&date_to=2017-06-28', 'time_med': 0.071, 'time_perc': 0.11707670997955279, 'count_perc': 25.0}])

    # def test_parser1(self):
    #     with self.assertRaises(SystemExit) as cm:
    #         content = ("""GET /api/v2/internal/html5/phantomjs/queue/?wait=1m HTTP/1.1" 1.140.178.176 -  - [29/Jun/2017:04:42:25 +0300] 200 22 "-" "Python-urllib/2.7" "-" "1498700545-1643579065-4707-9853341" "82545a302c27186d07" 60.093\n1.126.153.80 -  - [29/Jun/2017:04:42:25 +0300] "GET /api/v2/banner/25839458/statistic/outgoings/?date_from=2017-06-28&date_to=2017-06-28 HTTP/1.1" 200 40 "-" "-" "-" "1498700545-48424485-4707-9853342" "1835ae0f17f" ppp\n1.126.153.80 -  - [29/Jun/2017:04:42:26 +0300] "GET /api/v2/banner/22199839/statistic/outgoings/?date_from=2017-06-28&date_to=2017-06-28 HTTP/1.1" 200 40 "-" "-" "-" "1498700545-48424485-4707-9853344" "1835ae0f17f" 0.071\n1.169.137.128 -  - [29/Jun/2017:04:42:26 +0300] "GET /api/v2/group/5775925/banners HTTP/1.1" 200 1136 "-" "Configovod" "-" "1498700545-2118016444-4707-9853337" "712e90144abee9" ppp""")
    #         res = log_analyzer.parser(content)
    #         self.assertEqual(cm.exception.code, 1)

    def test_replace_word(self):
        with self.assertRaises(SystemExit) as cm:
            res = log_analyzer.replace_word("test", "report.html", "test", "20170630")
            self.assertEqual(cm.exception.code, 1)

if __name__ == '__main__':
    unittest.main()