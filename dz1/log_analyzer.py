#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import os
import sys
import json
import gzip
import datetime
import operator
import fileinput
import argparse
import re
from collections import namedtuple
import mimetypes
import string
# log_format ui_short '$remote_addr $remote_user $http_x_real_ip [$time_local] "$request" '
#                     '$status $body_bytes_sent "$http_referer" '
#                     '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" '
#                     '$request_time';

config = {
    "REPORT_SIZE": 3,
    "REPORT_DIR": "./reports",
    "LOG_DIR": "./log",
    "LOGGING_FILENAME": "u'nginx_parser_log",
    "PERCENT_ERROR": 60
}

LogFile = namedtuple('log_file', 'filename date')


def read_argument():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", help="path_file_config", default="./config.json")
    return parser.parse_args()


def read_config_files(path_file, default_config):
    config_dict = default_config.copy()
    try:
        with open(path_file, 'r') as infile:
            config_dict.update(json.loads(infile.read()))
    except (IOError, ValueError):
        logging.exception("no read files setting")
        return
    return config_dict


def search_last_file(log_dir):
    log_file = None
    filename = re.compile(r'^nginx-access-ui\.log-([\d]{8})[.]?([\S]*)')
    date_max = None
    if os.path.exists(log_dir):
        for file_dir in os.listdir(log_dir):
            filename_search = filename.match(file_dir)
            if filename_search:
                date = filename_search.groups()[0]
                try:
                    date = datetime.datetime.strptime(date, '%Y%m%d')
                except ValueError:
                    continue
                if date_max is None or date > date_max:
                    date_max = date
                    log_file = LogFile(filename=file_dir, date=date)                  
    return log_file


def check_report(dir_report, date_infiles):
    date = date_infiles.strftime('%Y.%m.%d')
    som_files = 'report-%s.html' % (date)
    return os.path.exists(os.path.join(dir_report, som_files))
    

def read_log_files(max_files, log_dir):
    if max_files.split(".")[-1] == "gz":
        open_log = gzip.open
    else:
        open_log = open
    with open_log(os.path.join(log_dir, max_files), 'rb') as log:
        for line in log:
            yield line.decode('utf-8')


def sorted_url_time(precent_error, read_log):
    error = 0
    total_count = 0
    total_time = 0
    dict_url_time = {}
    for i in read_log:
        line_analyze = i.split(" ")
        try:
            url = line_analyze[7]
            time = float(line_analyze[-1])
            if len(url.split("/")) < 2:
                error += 1
            if url in dict_url_time:
                all_time = dict_url_time[url]["all_time"]  #  {url:["time", "time" .....], }
                all_time.append(time)
                dict_url_time[url]["all_time"] = all_time
            else:
                dict_url_time[url] = {"all_time": [time]}
        except (ValueError,IndexError):
            error += 1
            continue
        total_count = total_count + 1
        total_time = total_time + time
    if (100.0 * error) / total_count > precent_error:  # error threshold check
        logging.error("high percentage of errors")
        raise Exception("high percentage of errors")
    return dict_url_time, error, total_count, total_time

def create_list_dict(dict_url_time, error, total_count, total_time):
    list_of_dict = []
    for url in dict_url_time:
        spis_time = dict_url_time[url]['all_time']
        count = len(spis_time)
        count_perc = (100.0 * count) / total_count
        time_sum = sum(spis_time)
        time_perc = (100.0 * time_sum) / total_time
        time_avg = time_sum / count
        time_max = max(spis_time)
        if count % 2 == 1:
            time_med = sorted(spis_time)[count//2]
        else: 
            time_med = sum(sorted(spis_time)[count//2:count//2+1])/2.0
        list_of_dict.append({"url": url.encode("UTF-8"),
                                "count": count,
                                "count_perc": count_perc,
                                "time_sum": time_sum,
                                "time_perc": time_perc,
                                "time_avg": time_avg,
                                "time_max": time_max,
                                "time_med": time_med})
    list_of_dict.sort(key=operator.itemgetter('time_sum'), reverse=True)
    return list_of_dict



def render_report(infile, new_word, new_name):
    name_new_files = os.path.join(config["REPORT_DIR"], "report-%s.html" % (new_name.strftime('%Y.%m.%d')))
    with open (infile, 'r') as f1, open(name_new_files,'w') as f2:
        f2.write(f1.read().replace("$table_json", str(new_word)))



def main(conf):
    arguments = read_argument()
    sett = read_config_files(arguments.config, config)
    if not sett:
        return
    
    logging.basicConfig(filename=sett["LOGGING_FILENAME"],
                        level=logging.INFO,
                        format='[%(asctime)s] %(levelname)s %(message)s',
                        datefmt='%Y/%m/%d %H:%M:%S')
    logging.info("----START----")
    dir_files = search_last_file(sett['LOG_DIR'])
    if not check_report(sett["REPORT_DIR"], dir_files.date):
        url_time, error, total_count, total_time = sorted_url_time(sett["PERCENT_ERROR"], read_log_files(dir_files.filename, sett["LOG_DIR"]))
        stat = create_list_dict(url_time, error, total_count, total_time)[:sett["REPORT_SIZE"]]
        render_report(os.path.abspath("report.html"), stat, dir_files.date)
        logging.info("----END----")
    else:
        logging.info("the file for that date exists")
        return

if __name__ == "__main__":
    try:
        main(config)
    except SystemExit:
        logging.exception("exit")
    except Exception as e:
        logging.exception("Unexpected error: %s" % (e))
